import java.sql.SQLOutput;
import java.util.Arrays;

public class Main {

    public static  void sort(int array[]) {
        for (int i = array.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (array[j] > array[j + 1]) {
                    int temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
            }
        }
    }
    public static void main(String[] args) {
        int array[][] = {
                {23, 20, 11, 15, 30, -10},
                {3, 0, 1, 5, 0, 0},
                {2, 2, 1, 1, 3, -1}};

        for (int i = 0; i < array.length; i++) {
            sort(array[i]);
        }
        System.out.println(Arrays.toString(array[0]));
        System.out.println(Arrays.toString(array[1]));
        System.out.println(Arrays.toString(array[2]));



    }
}
