public class Program {
    public static int fact(int n) {
        System.out.println("->> fact with " + n);
        if (n == 0) {
            return 1;
        }
        return fact( n: n - 1) * n;
    }
    public static void main(String[] args) {
        int number = fact( n: 5);
        System.out.println(number);
    }
}
